import os
import logging
import time
import datetime
import uuid
import requests
import telegram
import sys
from telegram import Update
from telegram.ext import ApplicationBuilder, CommandHandler, ContextTypes
from config import config_from_yaml

#TODO:
cfg = config_from_yaml("./skybeard-telegram-config.yaml", read_from_file=True)


#configure logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

CHAT_CONTEXTS = dict()

def generate_dalai_request(txt, model):
    # A request dictionary
    params= cfg.model_params.as_dict()
    request_d = {'debug': False,
                'id': str(uuid.uuid4()),
                'model': model,
                'prompt': txt,
                }
    request_d = {**request_d, **params}
    return request_d

def run_chatbot(txt, model):
    data = generate_dalai_request(txt, model)
    job_id = data['id']
    headers = {'Content-Type': 'application/json'}
    socket_url = os.environ['SKB_JOB_SERVER_HOST']
    logging.info("Socket URL: {}".format(socket_url))
    url = socket_url + '/send'
    logging.info(url)
    response = requests.post(url, headers=headers, json=data)
    if response.status_code !=200:
        logging.error(response.text)
        return "Oh no, I can't contact my brain"
    logging.info('Inference job submitted with ID: {}'.format(job_id))
    data = {'job_id': job_id}
    url = socket_url + '/status'
    headers = {}
    status = False
    to_completion = True

    polling_params = cfg.polling.as_dict()

    poll_delay = polling_params.get('delay', 5)
    poll_strikes = polling_params.get('strikes', 6)
    strike_counter = 0
    poll_counter = 0
    chunks = 0
    previous_chunks = 0
    while True:
        poll_counter+=1
        try:
            poll_response = requests.get(url, params=data).json()
            status = poll_response['status']
            exists = poll_response['exists']
            chunks = poll_response['chunks']

            if chunks == previous_chunks:
                strike_counter+=1
            else:
                strike_counter = 0
            previous_chunks = chunks

            logging.info('({} - {}) ID: {} - Exists: {} - Completed: {} - Chunks: {}'.format(
                poll_counter,
                strike_counter,
                job_id,
                exists,
                status,
                chunks))
            if status is True:
                break
            if strike_counter >= poll_strikes:
                to_completion = False
                break
        except Exception as e:
            logging.error(e)
        
        time.sleep(poll_delay)

    model_response = ''
    try:
        # even if didn't complete after 5 minutes, get whatever result there might be anyway
        url = socket_url +  '/results'
        response = requests.get(url, params=data)
        result_json = response.json()
        logging.info(result_json)
        model_response = result_json.get('response_string', '')
    except Exception as e:
        logging.error(e)
        return "Sorry it looks like I produced something so nonsensical I can't send it"
    return model_response


def parse_skybeard_response(text):
    text = text.split('### Response:')[-1].replace('### Response:', '').replace('<end>', '').replace('[end of text]', '').strip()
    #in case of debug mode
    if 'llama_print_timings:'in text:
        text = text.split('llama_print_timings:')[0]
    logging.info(text)
    if text == '':
        text = "My model didn't return anything"
    return text

def parse_darkbeard_response(text):
    text = text.split('Darkskybeard:')[-1].replace('<end>', '').replace('[end of text]', '').strip()
    #in case of debug mode
    if 'llama_print_timings:'in text:
        text = text.split('llama_print_timings:')[0]
    logging.info(text)
    if text == '':
        text = "My model didn't return anything"
    return text

def parse_drbeard_response(text):
    text = text.split('Darkskybeard:')[-1].replace('<end>', '').replace('[end of text]', '').strip()
    #in case of debug mode
    if 'llama_print_timings:'in text:
        text = text.split('llama_print_timings:')[0]
    logging.info(text)
    if text == '':
        text = "My model didn't return anything"
    return text

def template_input(context, update, input_text):
    chat = update.message.chat
    bot = context.bot.bot
    user = update.message.from_user

    template = []
    template.append(CHAT_CONTEXTS['base'].format(
        bot.first_name,
        user.first_name))
    
    if chat.type != telegram.constants.ChatType.PRIVATE:
        template.append(CHAT_CONTEXTS['groupchat'].format(chat.title))
    template.append(CHAT_CONTEXTS['custom'])
    template.append(input_text)
    return ' '.join(template)

## TODO: generic template loading and formatting
def load_template(txt, user, template):
    
    if template in CHAT_CONTEXTS:
        logging.info('Loading {} template'.format(template))
        if template == 'darkbeard' or template == 'drbeard':
            return CHAT_CONTEXTS[template].format(user= user, prompt=txt)
        elif template == 'horoscope':
            week = datetime.datetime.now().isocalendar()[1]
            year = datetime.datetime.today().year
            return CHAT_CONTEXTS[template].format(week=week, year=year, prompt=txt)
        return CHAT_CONTEXTS[template].format(txt)
        
    else:
        logging.warning('Template {} not found. Loading default'.format(template))       
        return CHAT_CONTEXTS['default'].format(txt)
        
# skybeard command handler
async def ask(update: Update, context: ContextTypes.DEFAULT_TYPE):
    quote = update.message.chat.type != telegram.constants.ChatType.PRIVATE
    await update.message.reply_text("My new command is now '/skybeard'", quote=quote)

# skybeard command handler
async def skybeard(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_chat_action(
        chat_id=update.effective_message.chat_id,
        action=telegram.constants.ChatAction.TYPING)
    quote = update.message.chat.type != telegram.constants.ChatType.PRIVATE
    result = ''
    reply = ''
    user = update.message.from_user.first_name
    try:
        input_text = ' '.join(context.args)
        if input_text is None or input_text == '':
            await update.message.reply_text("what?", quote=quote)
            return    
        template = 'default'
        model = 'llama.13b'
        logging.info('Requesting {} model'.format(model))
        #TODO: re-implement dynamic templating
        #full_prompt = template_input(context, update, input_text)
        full_prompt = load_template(input_text, user, template)

        logging.info('PROMPT: ' + full_prompt)
        result = run_chatbot(full_prompt, model)
        reply = parse_skybeard_response(result)
    except Exception as e:
        logging.info(e)
        await update.message.reply_text("oops something went wrong", quote=quote)
        return

    await update.message.reply_text(reply, quote=quote)

# darkbeard command handler
async def darkbeard(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_chat_action(
        chat_id=update.effective_message.chat_id,
        action=telegram.constants.ChatAction.TYPING)
    quote = update.message.chat.type != telegram.constants.ChatType.PRIVATE
    result = ''
    reply = ''
    user = update.message.from_user.first_name
    try:
        input_text = ' '.join(context.args)
        if input_text is None or input_text == '':
            await update.message.reply_text("what?", quote=quote)
            return    
        template = 'darkbeard'
        model = 'llama.13b'
        logging.info('Requesting {} model'.format(model))
        full_prompt = load_template(input_text, user, template)

        logging.info('PROMPT: ' + full_prompt)
        result = run_chatbot(full_prompt, model)
        reply = parse_darkbeard_response(result)
    except Exception as e:
        logging.info(e)
        await update.message.reply_text("oops something went wrong", quote=quote)
        return

    await update.message.reply_text(reply, quote=quote)

# drbeard command handler
async def drbeard(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_chat_action(
        chat_id=update.effective_message.chat_id,
        action=telegram.constants.ChatAction.TYPING)
    quote = update.message.chat.type != telegram.constants.ChatType.PRIVATE
    result = ''
    reply = ''
    user = update.message.from_user.first_name
    try:
        input_text = ' '.join(context.args)
        if input_text is None or input_text == '':
            await update.message.reply_text("what?", quote=quote)
            return    
        template = 'drbeard'
        model = 'llama.13b'
        logging.info('Requesting {} model'.format(model))
        full_prompt = load_template(input_text, user, template)

        logging.info('PROMPT: ' + full_prompt)
        result = run_chatbot(full_prompt, model)
        reply = parse_drbeard_response(result)
    except Exception as e:
        logging.info(e)
        await update.message.reply_text("oops something went wrong", quote=quote)
        return

    await update.message.reply_text(reply, quote=quote)

# drbeard command handler
async def horoscope(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_chat_action(
        chat_id=update.effective_message.chat_id,
        action=telegram.constants.ChatAction.TYPING)
    quote = update.message.chat.type != telegram.constants.ChatType.PRIVATE
    result = ''
    reply = ''
    user = update.message.from_user.first_name
    try:
        input_text = ' '.join(context.args)
        if input_text is None or input_text == '':
            await update.message.reply_text("what?", quote=quote)
            return    
        template = 'horoscope'
        model = 'llama.13b'
        logging.info('Requesting {} model'.format(model))
        full_prompt = load_template(input_text, user, template)

        logging.info('PROMPT: ' + full_prompt)
        result = run_chatbot(full_prompt, model)
        reply = parse_skybeard_response(result)
    except Exception as e:
        logging.info(e)
        await update.message.reply_text("oops something went wrong", quote=quote)
        return

    await update.message.reply_text(reply, quote=quote)
     
if __name__ == '__main__':
    
    logging.info("Job server host set as: {}".format(os.environ['SKB_JOB_SERVER_HOST']))
    
    for f in os.listdir('contexts'):
        filename = os.fsdecode(f)
        if filename.endswith(".txt"):
            context_name = os.path.splitext(os.path.basename(filename))[0]
            with open(os.path.join('contexts', filename)) as context_file: 
                CHAT_CONTEXTS[context_name] = context_file.read()

    telegram_config = cfg.telegram.as_dict()
    application = ApplicationBuilder().token(telegram_config['api_token']).build()
    
    ask_handler = CommandHandler('ask', ask)
    application.add_handler(ask_handler)

    skybeard_handler = CommandHandler('skybeard', skybeard)
    application.add_handler(skybeard_handler)

    darkbeard_handler = CommandHandler('darkbeard', darkbeard)
    application.add_handler(darkbeard_handler)

    drbeard_handler = CommandHandler('drbeard', drbeard)
    application.add_handler(drbeard_handler)

    horoscope_handler = CommandHandler('horoscope', horoscope)
    application.add_handler(horoscope_handler)
    
    application.run_polling()