# Quickstart

- Put the config.env somewhere on your host machine and edit it to add your Telegram bot token

### Build
```
docker build --tag skybeard-ai-tg .
```
### Run
```
docker run --network host --env-file path/to/config.env skybeard-ai-tg
```

_Coming Soon: Full docker compose for Skybeard AI, Skybeard Job Server and Dali_
